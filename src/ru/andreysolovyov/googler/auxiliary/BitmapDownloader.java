package ru.andreysolovyov.googler.auxiliary;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

public class BitmapDownloader {

	public static Bitmap downloadBitmap(String URL) {
		Bitmap bitmap = null;
		Log.d("HELLO123", "BIT_DOWN 1 " + URL);
		try {
			URL urlConnection = new URL(URL);
			HttpURLConnection connection = (HttpURLConnection) urlConnection
					.openConnection();
			connection.setDoInput(true);
			connection.connect();
			
			Log.d("HELLO123", "BIT_DOWN 2");
			
			InputStream input = connection.getInputStream();

			bitmap = BitmapFactory.decodeStream(input);
			Log.d("HELLO123", "BIT_DOWN 3");
		} catch (MalformedURLException mue) {
			Log.e("HELLO123", "�������� ������ �������");
		} catch (IOException ioe) {
			Log.e("HELLO123", "������ �������� ����������");
			ioe.printStackTrace();
		}
		return bitmap;
	}
}
