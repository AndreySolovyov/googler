package ru.andreysolovyov.googler.activities;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import ru.andreysolovyov.googler.R;
import ru.andreysolovyov.googler.R.id;
import ru.andreysolovyov.googler.R.layout;
import ru.andreysolovyov.googler.R.menu;
import ru.andreysolovyov.googler.adapters.ImageListAdapter;
import ru.andreysolovyov.googler.auxiliary.BitmapDownloader;
import ru.andreysolovyov.googler.model.ImageRepresentation;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

public class ImagesSearchResultActivity extends Activity {

	public static final String IMAGE_KEY = "image";

	private GridView imagesFoundGridView;
	private ImageListAdapter adapter = new ImageListAdapter(this);

	private ArrayList<ImageRepresentation> imagesList;

	private LruCache<String, Bitmap> bitmapCache;

	Handler uiHandler = new Handler();

	private OnItemClickListener onItemClicklistener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			final Intent imageZoomIntent = new Intent(ImagesSearchResultActivity.this, ImageZoomActivity.class);

			Log.d("HELLO123", "ZOOM1");
			ImageView imageToZoom = (ImageView) view;

			final String URL = (String) imageToZoom.getTag();

			Bitmap storedBitmap = bitmapCache.get(URL);
			final ProgressDialog fullImageLoadingDialog = ProgressDialog.show(
					ImagesSearchResultActivity.this, "���������", "�������� �����������");
			
			if (storedBitmap == null) {
				
				Thread t = new Thread() {
					Bitmap bitmap;
					
					public void run() {
						bitmap = BitmapDownloader.downloadBitmap(URL);
						final byte[] byteArray = getBytesFromBitmap(bitmap);
						bitmapCache.put(URL, bitmap);
						uiHandler.post(new Runnable() {
							public void run() {
								fullImageLoadingDialog.dismiss();
								imageZoomIntent.putExtra(IMAGE_KEY, byteArray);
								Log.d("HELLO123", "ZOOM2");
								startActivity(imageZoomIntent);
								Log.d("HELLO123", "ZOOM3");
							}
						});
					}
				};
				t.start();
			}
			else {
				fullImageLoadingDialog.dismiss();
				byte[] byteArray = getBytesFromBitmap(storedBitmap);
				imageZoomIntent.putExtra(IMAGE_KEY, byteArray);
				Log.d("HELLO123", "ZOOM2");
				startActivity(imageZoomIntent);
			}
		}
	};

	
	private byte[] getBytesFromBitmap(Bitmap bitmap){
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
		
		byte[] byteArray = stream.toByteArray();
		
		return byteArray;
	}
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_images_search_result);

		Intent intent = getIntent();
		setTitle("����� ��: "
				+ intent.getExtras().getString(
						SearchQueryInputActivity.SEARCH_QUERY_KEY));

		imagesList = intent
				.getParcelableArrayListExtra(SearchQueryInputActivity.IMAGES_FOUND_KEY);
		
		int i = 0;
		for(ImageRepresentation ir : imagesList){
			Log.d("HELLO123",  " ����� "  + i++ + " ��� :" + ir.getURL());
		}

		//Toast.makeText(this, imagesList.get(5).getTbURL(), 2000).show();

		imagesFoundGridView = (GridView) findViewById(R.id.imagesFoundGridFiew);
		imagesFoundGridView.setOnItemClickListener(onItemClicklistener);

		if (bitmapCache == null) {
			initCache();
		}

		adapter.setImages(imagesList);
		adapter.setBitmapCache(bitmapCache);
		imagesFoundGridView.setAdapter(adapter);
	}

	private void initCache() {
		final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
		final int cacheSize = maxMemory / 4;

		bitmapCache = new LruCache<String, Bitmap>(cacheSize) {
			@Override
			protected int sizeOf(String key, Bitmap bitmap) {
				return bitmap.getRowBytes() * bitmap.getHeight() / 1024;
			}
		};
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.images_search_results, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onCancel(View view) {
		finish();
	}
}
