package ru.andreysolovyov.googler.customViews;

import ru.andreysolovyov.googler.R;
import ru.andreysolovyov.googler.R.drawable;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

public class ZoomableImageView extends View {

	public ZoomableImageView(Context context) {
		super(context);
		detector = new ScaleGestureDetector(getContext(), new ScaleListener());
		this.context = context;
	}

	public ZoomableImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		detector = new ScaleGestureDetector(getContext(), new ScaleListener());
		this.context = context;
	}

	public ZoomableImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		detector = new ScaleGestureDetector(getContext(), new ScaleListener());
		this.context = context;
	}

	private Bitmap bitmap;
	private Paint paint = new Paint();
	private boolean justOpened = true;

	/*
	 * private SimpleOnScaleGestureListener scaleListener = new
	 * SimpleOnScaleGestureListener(){ public boolean
	 * onScale(ScaleGestureDetector detector){ scale *=
	 * detector.getScaleFactor(); scale = Math.max(0.5f, Math.min(scale, 5.0f));
	 * matrix.setScale(scale, scale);
	 * zoomableImageView.setScaleType(ScaleType.MATRIX);
	 * zoomableImageView.setImageMatrix(matrix); return true; } };
	 */

	private class ScaleListener extends
			ScaleGestureDetector.SimpleOnScaleGestureListener {
		public boolean onScale(ScaleGestureDetector detector) {
			scaleFactor *= detector.getScaleFactor();
			scaleFactor = Math.max(MIN_ZOOM, Math.min(scaleFactor, MAX_ZOOM));
			return true;
		}
	}

	Context context;

	private Matrix matrix = new Matrix();

	private static float MIN_ZOOM = 0.5f;
	private static float MAX_ZOOM = 5f;

	private float scaleFactor = 1.f;
	private ScaleGestureDetector detector;

	private static int MODE_NONE = 0;
	private static int MODE_DRAG = 1;
	private static int MODE_ZOOM = 2;

	private int mode;
	private boolean dragged;

	private float startX = 0f;
	private float startY = 0f;

	private float translateX = 0f;
	private float translateY = 0f;

	private float previousTranslateX = 0f;
	private float previousTranslateY = 0f;

	private int displayWidth;
	private int displayHeight;
	
	
	private int bitmapWidth;
	private int bitmapHeight;

	private void setWidthAndHeight(Context context) {
		WindowManager windowManager = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		
		displayWidth = windowManager.getDefaultDisplay().getWidth();
		displayHeight = windowManager.getDefaultDisplay().getHeight();

		if (bitmap == null) {
			setBitmap(BitmapFactory.decodeResource(context.getResources(),
					R.drawable.sample_7));
		}
		
		boolean landscapeScreenedDevice = displayWidth > displayHeight;

		bitmapWidth = getBitmap().getWidth();
		bitmapHeight = getBitmap().getHeight();
		
		Toast.makeText(context, "H " + bitmapHeight + "\nW " + bitmapWidth, 2000).show();

		//float tmpScaleFactor = 1f;
		
		float wRatio = (float) bitmapWidth / displayWidth;
		float hRatio = (float) bitmapHeight / displayHeight;
		
		float biggestRatio = wRatio > hRatio ? wRatio : hRatio;
		
		if(biggestRatio > 0){
			scaleFactor = 1 / biggestRatio;
		}
/*
		if (bitmapWidth > displayWidth || bitmapHeight > displayHeight) {
			if (bitmapWidth > displayWidth) {
				Log.e("HELLO123", "ZV1");
				scaleFactor = (float) displayWidth / bitmapWidth;
			}
			if (bitmapHeight > displayHeight) {
				Log.e("HELLO123", "ZV2");
				tmpScaleFactor =  (float) displayHeight / bitmapHeight ;
			}
			scaleFactor = scaleFactor < tmpScaleFactor ? scaleFactor
					: tmpScaleFactor;
		} else {
			if (bitmapWidth > bitmapHeight) {
				Log.e("HELLO123", "ZV3");
				scaleFactor = (float) displayWidth / bitmapWidth;
			} else {
				Log.e("HELLO123", "ZV4");
				scaleFactor = (float) displayHeight / bitmapHeight;
			}
		}
*/
	//	MIN_ZOOM = scaleFactor < 1 ? scaleFactor/2 : 0.5f;
		
		Toast.makeText(context,
				"SC_F " + scaleFactor + "\nMIN_ZOOM " + MIN_ZOOM, 2000).show();

		/*
		 * if(bitmapWidth > displayWidth || bitmapHeight > displayHeight){
		 * 
		 * }
		 */
	}

	public boolean onTouchEvent(MotionEvent event) {

		switch (event.getAction() & MotionEvent.ACTION_MASK) {

		case MotionEvent.ACTION_DOWN:
			mode = MODE_DRAG;

			startX = event.getX() - previousTranslateX;
			startY = event.getY() - previousTranslateY;
			break;

		case MotionEvent.ACTION_MOVE:
			translateX = event.getX() - startX;
			translateY = event.getY() - startY;

			double distance = Math
					.sqrt(Math.pow(
							event.getX() - (startX + previousTranslateX), 2)
							+ Math.pow(event.getY()
									- (startY + previousTranslateY), 2));

			if (distance > 0) {
				dragged = true;
			}
			break;

		case MotionEvent.ACTION_POINTER_DOWN:
			mode = MODE_ZOOM;
			break;

		case MotionEvent.ACTION_UP:
			mode = MODE_NONE;
			dragged = false;

			previousTranslateX = translateX;
			previousTranslateY = translateY;
			break;

		case MotionEvent.ACTION_POINTER_UP:
			mode = MODE_DRAG;

			previousTranslateX = translateX;
			previousTranslateY = translateY;

			break;
		}

		detector.onTouchEvent(event);

		if ((mode == MODE_DRAG && dragged) || mode == MODE_ZOOM) {
			invalidate();
		}
		return true;
	}

	public void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		if (justOpened) {
			translateX = displayWidth / 2;
			translateY = displayHeight / 2;

			previousTranslateX = translateX;
			previousTranslateY = translateY;

			justOpened = false;
		}

		matrix.reset();
		matrix.postTranslate(-getBitmap().getWidth() / 2.0f, -getBitmap()
				.getHeight() / 2.0f);

		matrix.postScale(scaleFactor, scaleFactor);
		matrix.postTranslate(translateX, translateY);

		canvas.drawBitmap(getBitmap(), matrix, paint);

		// canvas.translate(translateX/scaleFactor, translateY/scaleFactor);
		// canvas.restore();

	}

	public Bitmap getBitmap() {
		return bitmap;
	}

	public void setBitmap(Bitmap bitmap) {
		this.bitmap = bitmap;
		setWidthAndHeight(context);
	}
}
