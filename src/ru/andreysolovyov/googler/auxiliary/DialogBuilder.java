package ru.andreysolovyov.googler.auxiliary;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

public class DialogBuilder {
	public static AlertDialog makeErrorDialog(Context context, String title, String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(message);
		builder.setTitle(title);
		builder.setPositiveButton("��", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		AlertDialog dialog = builder.create();
		return dialog;
	}
	
	public static ProgressDialog makePorgressDialog(Context context,String title, String message) {
		return ProgressDialog.show(context, title, message);
	}
}
