package ru.andreysolovyov.googler.activities;

import ru.andreysolovyov.googler.R;
import ru.andreysolovyov.googler.R.id;
import ru.andreysolovyov.googler.R.layout;
import ru.andreysolovyov.googler.R.menu;
import ru.andreysolovyov.googler.customViews.ZoomableImageView;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


public class ImageZoomActivity extends Activity {

	private ZoomableImageView zoomableImageView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_image_zoom);
		zoomableImageView = (ZoomableImageView) findViewById(R.id.zoomableImageView);
	//	scaleGestureDetector = new ScaleGestureDetector(this, scaleListener);
		Log.d("HELLO123", "ZOOM4");
		
		Intent intent = getIntent();
		Log.d("HELLO123", "ZOOM5");
		
		byte[] bitmapBytes = intent.getByteArrayExtra(ImagesSearchResultActivity.IMAGE_KEY);
		
		Bitmap imageToZoomBitmap = BitmapFactory.decodeByteArray(bitmapBytes, 0, bitmapBytes.length); 
		
		Log.d("HELLO123", "GOT_A_BITMAP");
		
     	zoomableImageView.setBitmap(imageToZoomBitmap);
		//zoomableImageView.setScaleType(ScaleType.MATRIX);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.image_zoom, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void onCancel(View view){
		finish();
	}
}
