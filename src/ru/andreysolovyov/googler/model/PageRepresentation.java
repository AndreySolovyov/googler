package ru.andreysolovyov.googler.model;

import android.os.Parcel;
import android.os.Parcelable;

public class PageRepresentation implements Parcelable{
	private String title;
	private String visibleURL;
	private String URL;
	
	public PageRepresentation(String title, String visibleURL, String URL){
		this.title = title;
		this.visibleURL = visibleURL;
		this.URL = URL;
	}
	
	public PageRepresentation(Parcel source){
        title = source.readString();
        visibleURL = source.readString();
        URL = source.readString();
	}
	
	public String getTitle(){
		return title;
	}
	
	public String getURL(){
		return URL;
	}
	
	
	public void setTitle(String title){
		this.title = title;
	}
	
	public void setURL(String URL){
		this.URL = URL;
	}
	
	public String getVisibleURL(){
		return visibleURL;
	}
	
	public void setVisibleURL(String visibleURL){
		this.visibleURL = visibleURL;
	}

	@Override
	public int describeContents() {
		return CONTENTS_FILE_DESCRIPTOR;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(title);
		dest.writeString(visibleURL);
		dest.writeString(URL);
	}
	
	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public PageRepresentation createFromParcel(Parcel in) {
            return new PageRepresentation(in); 
        }

		@Override
		public Object[] newArray(int size) {
			return new PageRepresentation[size];
		}
    };
}
