package ru.andreysolovyov.googler.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class RecentQueriesOpenHelper extends SQLiteOpenHelper {

	
	public RecentQueriesOpenHelper(Context context) {
		super(context, RecentQueriesDatabaseHelper.DATABASE_NAME, null, RecentQueriesDatabaseHelper.DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL("CREATE TABLE " + RecentQueriesDatabaseHelper.TABLE_NAME + "( " 
				+ RecentQueriesDatabaseHelper.SEARCH_QUERY_ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
				+ RecentQueriesDatabaseHelper.SEARCH_QUERY_TIME + " DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, "
				+ RecentQueriesDatabaseHelper.SEARCH_QUERY_TEXT + " TEXT NOT NULL )");
	}

	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
		database.execSQL("DROP TABLE IF EXISTS " + RecentQueriesDatabaseHelper.TABLE_NAME);
		onCreate(database);
	}

}
