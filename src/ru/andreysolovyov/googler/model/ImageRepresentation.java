package ru.andreysolovyov.googler.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ImageRepresentation implements Parcelable{
	
	private String tbURL;
	private String URL;
	private String imageId;
	
	public ImageRepresentation(String imageId, String tbURL, String URL){
		this.imageId = imageId;
		this.tbURL = tbURL;
		this.URL = URL;
	}
	
	public ImageRepresentation(Parcel source){
		imageId = source.readString();
		tbURL = source.readString(); 
		URL = source.readString();
	}
	
	public String getURL() {
		return URL;
	}

	public void setURL(String URL) {
		this.URL = URL;
	}

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(imageId);
		dest.writeString(tbURL);
		dest.writeString(URL);
	}
	
	public String getTbURL() {
		return tbURL;
	}

	public void setTbURL(String tbURL) {
		this.tbURL = tbURL;
	}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public ImageRepresentation createFromParcel(Parcel dest) {
            return new ImageRepresentation(dest); 
        }

		@Override
		public Object[] newArray(int size) {
			return new ImageRepresentation[size];
		}
    };
}
