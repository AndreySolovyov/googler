package ru.andreysolovyov.googler.auxiliary;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Random;

import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ru.andreysolovyov.googler.R;
import ru.andreysolovyov.googler.R.drawable;
import ru.andreysolovyov.googler.model.ImageRepresentation;
import ru.andreysolovyov.googler.model.PageRepresentation;

import android.util.Log;

public class GoogleSearchAPIWorker {

	static int counter = 0;

	private static final String PAGES_SEARCH_URL = "http://ajax.googleapis.com/ajax/services/search/web?v=1.0";
	private static final String IMAGES_SEARCH_URL = "http://ajax.googleapis.com/ajax/services/search/images?v=1.0";

	private static final String RSZ_STRING = "&rsz=";
	private static final String START_STRING = "&start=";
	private static final String QUERY_STRING = "&q=";

	private static final int PAGES_COUNT = 20;
	private static final int IMAGES_COUNT = 60;

	private static final int GOOGLE_MAX_SEARCH_RESPONSE = 8;

	private static final int pageFullRequestsCount;
	private static final int numberOfPagesInResidualRequest;

	private static final int imageFullRequestsCount;
	private static final int numberOfImagesInResidualRequest;

	private int pagesStart = 0;
	private int imagesStart = 0;

	

	private GoogleSearchAPIWorker() {
	}

	public static final GoogleSearchAPIWorker instance = new GoogleSearchAPIWorker();

	static {
		numberOfPagesInResidualRequest = PAGES_COUNT
				% GOOGLE_MAX_SEARCH_RESPONSE;
		pageFullRequestsCount = (PAGES_COUNT - numberOfPagesInResidualRequest)
				/ GOOGLE_MAX_SEARCH_RESPONSE;

		numberOfImagesInResidualRequest = IMAGES_COUNT
				% GOOGLE_MAX_SEARCH_RESPONSE;
		imageFullRequestsCount = (IMAGES_COUNT - numberOfImagesInResidualRequest)
				/ GOOGLE_MAX_SEARCH_RESPONSE;
	}

	static private Integer[] mThumbIds = { R.drawable.sample_0,
			R.drawable.sample_1, R.drawable.sample_2, R.drawable.sample_3,
			R.drawable.sample_4, R.drawable.sample_5, R.drawable.sample_6,
			R.drawable.sample_7 };

	static Random r = new Random();

	// �� 1 ��� ����� �������� �� ����� 8 ������� ��� �������� (�����������
	// �����).
	// ���������� ������ �������� ��������� �� 8 �������. � ���� ������ ���� ����� ���������
	private String retreiveGivenNumberOfItems(String query, int rsz, int start) {
		StringBuilder resultBuilder = new StringBuilder();
		
		StringBuilder queryBuilder = new StringBuilder(query)
				.append(RSZ_STRING).append(rsz).append(START_STRING)
				.append(start);

		try {
			URL searchURL = new URL(queryBuilder.toString());
			Log.e("HELLO1234", "URL " + queryBuilder.toString());

			HttpURLConnection httpURLConnection = (HttpURLConnection) searchURL
					.openConnection();

			if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
				InputStreamReader inputStreamReader = new InputStreamReader(
						httpURLConnection.getInputStream());
				BufferedReader bufferedReader = new BufferedReader(
						inputStreamReader);
				String s = null;

				Log.e("HELLO123", "�������");

				while ((s = bufferedReader.readLine()) != null) {
					resultBuilder.append(s);
				}
				bufferedReader.close();
			}
		} catch (MalformedURLException mue) {
			Log.e("HELLO123", "�������� ������ �������");
		} catch (IOException ioe) {
			Log.e("HELLO123", "������ �������� ����������");
			ioe.printStackTrace();
		}
		return resultBuilder.toString();

	}

	private boolean parseResponseToPagesList(String jsonResponse,
			ArrayList<PageRepresentation> pages) {
		try {

			JSONArray jsonArrayResults = parseJSONResultsArray(jsonResponse);

			Log.d("HELLO123", "Number of results returned = "
					+ jsonArrayResults.length() + "\n\n");

			for (int i = 0; i < jsonArrayResults.length(); i++) {
				Log.d("HELLO123 COUNTER", "" + i);
				JSONObject jsonObjectPage = jsonArrayResults.getJSONObject(i);

				String title = jsonObjectPage.getString("title");
				String visibleURL = jsonObjectPage.getString("visibleUrl");
				String URL = jsonObjectPage.getString("url");

				PageRepresentation page = new PageRepresentation(title,
						visibleURL, URL);
				pages.add(page);

				Log.d("HELLO123", "title: " + title);
				Log.d("HELLO123", "url: " + URL);
			}
			return true;
		} catch (JSONException e) {
			Log.e("HELLO123", "������ �������������� ������");
			e.printStackTrace();
		}
		return false;
	}

	private JSONArray parseJSONResultsArray(String jsonResponse)
			throws JSONException {
		JSONObject jsonObject = new JSONObject(jsonResponse);
		JSONObject jsonObject_responseData = jsonObject
				.getJSONObject("responseData");
		JSONArray jsonArrayResults = jsonObject_responseData
				.getJSONArray("results");

		return jsonArrayResults;
	}

	private void parseResponseToImagesList(String jsonResponse,
			ArrayList<ImageRepresentation> images) {
		try {

			JSONArray jsonArrayResults = parseJSONResultsArray(jsonResponse);

			Log.d("HELLO123",
					"Number of iamges returned = " + jsonArrayResults.length()
							+ "\n\n");

			for (int i = 0; i < jsonArrayResults.length(); i++) {
				Log.d("HELLO123_COUNTER", "" + i);
				JSONObject jsonObjectImage = jsonArrayResults.getJSONObject(i);

				String imageId = jsonObjectImage.getString("imageId");
				String tbURL = jsonObjectImage.getString("tbUrl").replace("\\u0003", "="); // tbURL �� ����� �������� � �������� �������, ��� ���� ����� ������� �� \u0003
				String URL = jsonObjectImage.getString("url");

				ImageRepresentation page = new ImageRepresentation(imageId, tbURL, URL);
				images.add(page);

				Log.d("HELLO123", "imId: " + imageId);
				Log.d("HELLO123", "url: " + URL);
				Log.d("HELLO123", "title: " + jsonObjectImage.getString("titleNoFormatting"));
			}
		} catch (JSONException e) {
			Log.e("HELLO123", "������ �������������� ������");
			e.printStackTrace();
		}
	}

	private String prepareSearchQuery(String baseQuery, String searchItem) {
		try {
			searchItem = URLEncoder.encode(searchItem, HTTP.UTF_8);
		} catch (UnsupportedEncodingException uee) {
			Log.e("HELLO123", "������ �������������� �������");
		}

		Log.d("HELLO123", " ���� " + pageFullRequestsCount + " ����� ���� "
				+ numberOfPagesInResidualRequest);

		StringBuilder searchQueryBuilder = new StringBuilder(baseQuery).append(
				QUERY_STRING).append(searchItem);

		return searchQueryBuilder.toString();
	}

	public ArrayList<PageRepresentation> searchForPages(String searchItem) {
		ArrayList<PageRepresentation> pages = new ArrayList<PageRepresentation>();
		if (pagesStart == 0) {
			pagesStart = 1;
		} else {
			if (GOOGLE_MAX_SEARCH_RESPONSE - numberOfPagesInResidualRequest > 0) {
				pagesStart += numberOfPagesInResidualRequest + 1;
				parseResponseToPagesList(
						retreiveGivenNumberOfItems(
								prepareSearchQuery(PAGES_SEARCH_URL, searchItem),
								GOOGLE_MAX_SEARCH_RESPONSE
										- numberOfPagesInResidualRequest, pagesStart),
						pages);
			} else {
				pagesStart += GOOGLE_MAX_SEARCH_RESPONSE;
			}
		}

		for (int i = 0; i < pageFullRequestsCount; i++) {
			parseResponseToPagesList(
					retreiveGivenNumberOfItems(
							prepareSearchQuery(PAGES_SEARCH_URL, searchItem),
							GOOGLE_MAX_SEARCH_RESPONSE, pagesStart), pages);
			pagesStart += GOOGLE_MAX_SEARCH_RESPONSE;
		}

		if (numberOfPagesInResidualRequest != 0) {
			parseResponseToPagesList(
					retreiveGivenNumberOfItems(
							prepareSearchQuery(PAGES_SEARCH_URL, searchItem),
							numberOfPagesInResidualRequest, pagesStart), pages);
		}
		return pages;
		// pagesStart++;
	}

	public ArrayList<ImageRepresentation> searchForImages(String searchItem) {
		ArrayList<ImageRepresentation> images = new ArrayList<ImageRepresentation>();
		
		if (imagesStart == 0) {
			imagesStart = 1;
		} else {
			if (GOOGLE_MAX_SEARCH_RESPONSE - numberOfImagesInResidualRequest > 0) {
				imagesStart += numberOfImagesInResidualRequest + 1;
				parseResponseToImagesList(
						retreiveGivenNumberOfItems(
								prepareSearchQuery(IMAGES_SEARCH_URL, searchItem),
								GOOGLE_MAX_SEARCH_RESPONSE
										- numberOfImagesInResidualRequest, imagesStart),
						images);
			} else {
				imagesStart += GOOGLE_MAX_SEARCH_RESPONSE;
			}
		}
		
		for (int i = 0; i < imageFullRequestsCount; i++) {
			parseResponseToImagesList(
					retreiveGivenNumberOfItems(
							prepareSearchQuery(IMAGES_SEARCH_URL, searchItem),
							GOOGLE_MAX_SEARCH_RESPONSE, imagesStart), images);
			imagesStart += GOOGLE_MAX_SEARCH_RESPONSE;
		}

		if (numberOfImagesInResidualRequest != 0) {
			parseResponseToImagesList(
					retreiveGivenNumberOfItems(
							prepareSearchQuery(IMAGES_SEARCH_URL, searchItem),
							numberOfImagesInResidualRequest, imagesStart), images);
		}

		return images;
	}

	static void populateMockPages(ArrayList<PageRepresentation> currentList) {
		if (currentList == null) {
			currentList = new ArrayList<PageRepresentation>();
		}
		for (int i = 0; i < 20; i++) {
			// currentList.add(new PageRepresentation("����� " + ++counter,
			// "http://www.google.ru/#newwindow=1&q=hi&start= " + counter));
		}
	}

	static void populateMockImages(ArrayList<Integer> currentList) {
		if (currentList == null) {
			currentList = new ArrayList<Integer>();
		}

		for (int i = 0; i < 60; i++) {
			currentList.add(mThumbIds[r.nextInt(8)]);
		}
	}

	public void reset() {
		pagesStart = 0;
		imagesStart = 0;
	}
}
