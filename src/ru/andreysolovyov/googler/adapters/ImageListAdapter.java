package ru.andreysolovyov.googler.adapters;

import java.util.ArrayList;

import ru.andreysolovyov.googler.activities.SearchQueryInputActivity;
import ru.andreysolovyov.googler.auxiliary.DialogBuilder;
import ru.andreysolovyov.googler.auxiliary.GoogleSearchAPIWorker;
import ru.andreysolovyov.googler.auxiliary.ImageLoadTask;
import ru.andreysolovyov.googler.model.ImageRepresentation;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Handler;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

public class ImageListAdapter extends BaseAdapter {

	private Handler uiHanlder = new Handler();

	private LruCache<String, Bitmap> bitmapCache;

	private boolean mayProceedLoading = true;
	
	//private AlertDialog noMoreResultsDialog;

	// references to our images
	// private ArrayList<Integer> mThumbIds = new ArrayList<Integer>();

	private ArrayList<ImageRepresentation> images = new ArrayList<ImageRepresentation>();

	private Context context;

	public ImageListAdapter(Context context) {
		this.context = context;
		//noMoreResultsDialog = DialogBuilder.makeErrorDialog(context, "������� ���������", "���������� ��� ��������� ����������");
	}

	public int getCount() {
		return images.size();
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return 0;
	}

	// create a new ImageView for each item referenced by the Adapter
	public View getView(int position, View convertView, ViewGroup parent) {
		Log.e("HELLO123", "������� �������� " + position);

		ImageView imageView;
		if (convertView == null) { // if it's not recycled, initialize some
									// attributes
			imageView = new ImageView(context);
			imageView.setLayoutParams(new GridView.LayoutParams(145, 145));
			imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
			imageView.setPadding(8, 8, 8, 8);

		} else {
			imageView = (ImageView) convertView;
		}

		setImageAtPosition(position, imageView);
		/*
		 * if (position > AVERAGE_ROW_COUNT_PER_SCREEN && position +
		 * AVERAGE_ROW_COUNT_PER_SCREEN < images.size()) {
		 * setImageAtPosition(position + AVERAGE_ROW_COUNT_PER_SCREEN,
		 * imageView); }
		 */

		if (position == images.size() - 1) {
			if (mayProceedLoading) {
				final ProgressDialog progressDialog = DialogBuilder.makePorgressDialog(
						context, "���������", "����������� �����������");
				progressDialog.show();

				Thread t = new Thread() {
					public void run() {
						// GoogleSearchAPIWorker.instance.reset();
						ArrayList<ImageRepresentation> moreImages = GoogleSearchAPIWorker.instance
								.searchForImages(SearchQueryInputActivity.searchQuery);
						if(!moreImages.isEmpty()){
							images.addAll(moreImages);
						}
						else{
							mayProceedLoading = false;
						}
						uiHanlder.post(new Runnable() {
							public void run() {
								if(mayProceedLoading){
									notifyDataSetChanged();
								}
								progressDialog.dismiss();
							}
						});
					}
				};
				t.start();
			}else {
				DialogBuilder.makeErrorDialog(context, "������� ���������", "���������� ��� ��������� ����������").show();
		}
		}
		return imageView;
	}

	public ArrayList<ImageRepresentation> getImages() {
		return images;
	}

	private void setImageAtPosition(int position, ImageView imageView) {
		String imageId = images.get(position).getImageId();
		Log.e("HELLO123", "������ ����� " + position);

		Bitmap imageBitmap = getBitmapFromMemCache(imageId);

		if (imageBitmap != null && imageId != null) {
			imageView.setImageBitmap(imageBitmap);
			Log.e("HELLO123", "������ ����� � ���� " + position);
		} else {
			String thumbnailURL = images.get(position).getTbURL();
			ImageLoadTask task = new ImageLoadTask(thumbnailURL, imageId,
					imageView, getBitmapCache());
			task.execute();
			Log.e("HELLO123", "����� ������ ������ " + position);
		}
		// imageView.setImageResource(mThumbIds.get(position));
		if (position == images.size() - 1) {
			// Toast.makeText(convertView.getContext(), "���!",
			// 2000).show();
			// GoogleSearchAPIWorker.populateMockImages(mThumbIds);
			// notifyDataSetChanged();
		}
		imageView.setTag(images.get(position).getURL());
	}

	public void setImages(ArrayList<ImageRepresentation> images) {
		this.images = images;
		notifyDataSetChanged();
	}

	public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
		if (getBitmapFromMemCache(key) == null) {
			getBitmapCache().put(key, bitmap);
		}
	}

	public Bitmap getBitmapFromMemCache(String key) {
		return getBitmapCache().get(key);
	}

	public LruCache<String, Bitmap> getBitmapCache() {
		return bitmapCache;
	}

	public void setBitmapCache(LruCache<String, Bitmap> bitmapCache) {
		this.bitmapCache = bitmapCache;
	}

}
