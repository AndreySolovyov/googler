package ru.andreysolovyov.googler.db;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


public class RecentQueriesDatabaseHelper {
	
public static final int DATABASE_VERSION = 2;
public static final int PURGE_DAYS_THRESHOLD = 7;

public static final String DATABASE_NAME = "googler.db";
public static final String  TABLE_NAME = "recent_queries";

public static final String SEARCH_QUERY_ID = "id";
public static final String SEARCH_QUERY_TIME = "query_time";
public static final String SEARCH_QUERY_TEXT = "query_text";

private static final String SELECT_DISTINCT_SEARCH_QUERY_TEXT = "SELECT DISTINCT " + SEARCH_QUERY_TEXT + " FROM " + TABLE_NAME;
private static final String INSERT_NEW_SEARCH_QUERY = "INSERT INTO " + TABLE_NAME + "('" + SEARCH_QUERY_TEXT + "') values ('?')";

//DELETE FROM lol WHERE query_time < (SELECT datetime('now','start of day', '-7 day'));
private static final String PURGE_OLD_SEARCH_QUERIES = "DELETE FROM " + TABLE_NAME + " WHERE " + SEARCH_QUERY_TIME + " < (SELECT datetime('now','start of day', '-" + PURGE_DAYS_THRESHOLD + " day'))";

private RecentQueriesOpenHelper openHelper;
private SQLiteDatabase database;

public RecentQueriesDatabaseHelper(Context context){
	openHelper = new RecentQueriesOpenHelper(context);
	database = openHelper.getWritableDatabase();
}

public void saveQuery(String query){
	database.execSQL(INSERT_NEW_SEARCH_QUERY.replaceFirst("\\?", query));
	//Log.d("HELLO123", "SQL ������� " + dbQuery);
}

public void purgeOldSearchQueries(){
	database.execSQL(PURGE_OLD_SEARCH_QUERIES);
}

public List<String> selectSearchQueriesAsList(){
	Cursor searchQueriesCursor = database.rawQuery(SELECT_DISTINCT_SEARCH_QUERY_TEXT, null);
	List<String> searchQueriesList = new ArrayList<String>();
	
	if(searchQueriesCursor.moveToFirst()){
		do{
			String searchQueryText = searchQueriesCursor.getString(0);
			searchQueriesList.add(searchQueryText);
		} while (searchQueriesCursor.moveToNext());
	}
	return searchQueriesList;
}
}
