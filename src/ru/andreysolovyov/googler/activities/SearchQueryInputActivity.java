package ru.andreysolovyov.googler.activities;
 
import java.util.ArrayList;
import java.util.List;

import ru.andreysolovyov.googler.R;
import ru.andreysolovyov.googler.R.id;
import ru.andreysolovyov.googler.R.layout;
import ru.andreysolovyov.googler.R.menu;
import ru.andreysolovyov.googler.auxiliary.DialogBuilder;
import ru.andreysolovyov.googler.auxiliary.GoogleSearchAPIWorker;
import ru.andreysolovyov.googler.db.RecentQueriesDatabaseHelper;
import ru.andreysolovyov.googler.model.ImageRepresentation;
import ru.andreysolovyov.googler.model.PageRepresentation;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.RadioButton;

public class SearchQueryInputActivity extends Activity {

	private Button searchButton;

	private RadioButton pagesSearchRadioButton;
	private RadioButton imagesSearchRadioButton;

	private AutoCompleteTextView searchQueryAutoCompleteTextView;
	private ArrayAdapter<String> storedSearchQueriesAutocompleteAdapter;
	private List<String> searchQueriesList;

	public static final String SEARCH_QUERY_KEY = "searchQuery";
	public static final String PAGES_FOUND_KEY = "pagesFound";
	public static final String IMAGES_FOUND_KEY = "imagesFound";

	private Handler uiHanlder = new Handler();
	private RecentQueriesDatabaseHelper dbHelper;
	
	
	public static String searchQuery;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_query_input);

		searchButton = (Button) findViewById(R.id.searchButton);

		pagesSearchRadioButton = (RadioButton) findViewById(R.id.pagesSearchRadioButton);
		imagesSearchRadioButton = (RadioButton) findViewById(R.id.imagesSearchRadioButton);
		searchQueryAutoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.searchQueryEditText);
		
		dbHelper = new RecentQueriesDatabaseHelper(this);
		purgeOldSearchQueries();
		searchQueriesList =  selectStoredQueries();
		storedSearchQueriesAutocompleteAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, searchQueriesList);
		searchQueryAutoCompleteTextView.setAdapter(storedSearchQueriesAutocompleteAdapter);
	}
	
	private List<String> selectStoredQueries(){
		return dbHelper.selectSearchQueriesAsList();
	}
	
	private void purgeOldSearchQueries(){
		dbHelper.purgeOldSearchQueries();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.serach_query_input, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onSearchButtonClicked(View v) {
		searchQuery = searchQueryAutoCompleteTextView.getText().toString().trim();
		
		if (searchQuery.length() > 0) {
			dbHelper.saveQuery(searchQuery);
			storedSearchQueriesAutocompleteAdapter.add(searchQuery);
			
			// title = title.length() > 0 ? title : SEARCH_TITLE_STUB;
	
			final ProgressDialog dialog = DialogBuilder.makePorgressDialog(this, "�����",
					"����������, ���������");

			if (pagesSearchRadioButton.isChecked()) {
				final Intent intent = new Intent(this, PagesSearchResultActivity.class);
				intent.putExtra(SEARCH_QUERY_KEY, searchQuery);
				
				Thread t = new Thread() {
					public void run() {
						GoogleSearchAPIWorker.instance.reset();
						final ArrayList<PageRepresentation> pagesFound = GoogleSearchAPIWorker.instance
								.searchForPages(searchQuery);
						uiHanlder.post(new Runnable() {
							public void run() {
								dialog.dismiss();
								intent.putParcelableArrayListExtra(PAGES_FOUND_KEY, pagesFound);
								startActivity(intent);
							}
						});
					}
				};
				t.start();

				
			} else if (imagesSearchRadioButton.isChecked()) {
				final Intent intent = new Intent(this, ImagesSearchResultActivity.class);
				intent.putExtra(SEARCH_QUERY_KEY , searchQuery);
				
				Thread t = new Thread() {
					public void run() {
						GoogleSearchAPIWorker.instance.reset();
						final ArrayList<ImageRepresentation> imagesFound = GoogleSearchAPIWorker.instance
								.searchForImages(searchQuery);
						uiHanlder.post(new Runnable() {
							public void run() {
								dialog.dismiss();
								intent.putParcelableArrayListExtra(IMAGES_FOUND_KEY, imagesFound);
								startActivity(intent);
							}
						});
					}
				};
				t.start();
			} else {
				dialog.dismiss();
				return;
			}
		} else {
			DialogBuilder.makeErrorDialog(this, "������", "������ ������ ������").show();
		}
	}
	

}
