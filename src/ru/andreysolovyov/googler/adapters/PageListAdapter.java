package ru.andreysolovyov.googler.adapters;

import java.util.ArrayList;

import ru.andreysolovyov.googler.R;
import ru.andreysolovyov.googler.R.id;
import ru.andreysolovyov.googler.R.layout;
import ru.andreysolovyov.googler.activities.SearchQueryInputActivity;
import ru.andreysolovyov.googler.auxiliary.DialogBuilder;
import ru.andreysolovyov.googler.auxiliary.GoogleSearchAPIWorker;
import ru.andreysolovyov.googler.model.PageRepresentation;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class PageListAdapter extends BaseAdapter {

	private ArrayList<PageRepresentation> pagesList = new ArrayList<PageRepresentation>();
	private String searchQuery;
	private Handler uiHanlder = new Handler();

	private boolean mayProceedLoading = true;

	private Context context;
	
//	private AlertDialog noMoreResultsDialog;

	public PageListAdapter(Context context) {
		this.context = context;
		//noMoreResultsDialog = DialogBuilder.makeErrorDialog(context, "������� ���������", "���������� ��� ��������� ����������");
	}

	@Override
	public int getCount() {
		return pagesList.size();
	}

	@Override
	public PageRepresentation getItem(int arg0) {
		return pagesList.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int index, View view, ViewGroup parent) {
		if (view == null) {
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());
			view = inflater.inflate(R.layout.page_representation_in_list_view,
					parent, false);
		}
		PageRepresentation page = pagesList.get(index);

		TextView pageTitleTextView = (TextView) view
				.findViewById(R.id.pageTitleTextView);
		TextView pageURLTextView = (TextView) view
				.findViewById(R.id.pageURLTextView);

		pageTitleTextView.setText(Html.fromHtml(page.getTitle()));
		pageURLTextView.setText(page.getVisibleURL());

		if (index == pagesList.size() - 1) {
			if (mayProceedLoading) {
				final ProgressDialog progressDialog = ProgressDialog.show(
						context, "���������", "����������� ��������");
				
				Toast.makeText(parent.getContext(), "���!", 2000).show();

				Thread t = new Thread() {
					public void run() {
						ArrayList<PageRepresentation> moreImages = GoogleSearchAPIWorker.instance
								.searchForPages(SearchQueryInputActivity.searchQuery);
						if (moreImages.isEmpty()) {
							mayProceedLoading = false;
						}
						pagesList.addAll(moreImages);
						uiHanlder.post(new Runnable() {
							public void run() {
								notifyDataSetChanged();
								progressDialog.dismiss();
							}
						});
					}
				};
				t.start();
			} else {
				DialogBuilder.makeErrorDialog(context, "������� ���������", "���������� ��� ��������� ����������").show();
			}
		}

		return view;
	}
	
	

	public ArrayList<PageRepresentation> getPagesList() {
		return pagesList;
	}

	public void setPagesList(ArrayList<PageRepresentation> pagesList) {
		this.pagesList = pagesList;
		notifyDataSetChanged();
	}

	public String getSearchQuery() {
		return searchQuery;
	}

	public void setSearchQuery(String searchQuery) {
		this.searchQuery = searchQuery;
	}

	private ProgressDialog makePorgressDialog(Context context, String title,
			String message) {
		return ProgressDialog.show(context, title, message);
	}

}
