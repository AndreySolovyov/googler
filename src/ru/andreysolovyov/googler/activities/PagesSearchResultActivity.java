package ru.andreysolovyov.googler.activities;

import java.util.ArrayList;

import ru.andreysolovyov.googler.R;
import ru.andreysolovyov.googler.R.id;
import ru.andreysolovyov.googler.R.layout;
import ru.andreysolovyov.googler.R.menu;
import ru.andreysolovyov.googler.adapters.PageListAdapter;
import ru.andreysolovyov.googler.model.PageRepresentation;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class PagesSearchResultActivity extends Activity {

	private ArrayList<PageRepresentation> pagesFound;
	private String searchQuery;
	

	private PageListAdapter pageListAdapter  = new PageListAdapter(this);
	{
		itemClickListener = new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
				PageRepresentation page = (PageRepresentation) adapter.getItemAtPosition(position);

				Toast.makeText(PagesSearchResultActivity.this, Uri.parse(page.getURL()).toString(), Toast.LENGTH_SHORT).show();
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(page.getURL()));
				startActivity(browserIntent);
			}
			
		};
    }
	
	//private OnScrollListener scrollListener;
	private OnItemClickListener itemClickListener;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pages_search_result);

		ListView foundPagesListView = (ListView) findViewById(R.id.pagesFoundList);
		
		foundPagesListView.setAdapter(pageListAdapter);
	//	foundPagesListView.setOnScrollListener(scrollListener);
		foundPagesListView.setOnItemClickListener(itemClickListener);
		
		Intent intent = getIntent();
		
		searchQuery = intent.getExtras().getString(SearchQueryInputActivity.SEARCH_QUERY_KEY);
		setTitle("����� ��: " + searchQuery);
		
		pagesFound = intent.getParcelableArrayListExtra(SearchQueryInputActivity.PAGES_FOUND_KEY);
		pageListAdapter.setSearchQuery(searchQuery);
		pageListAdapter.setPagesList(pagesFound);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.pages_search_result, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void onCancel(View view){
		finish();
	}
}
