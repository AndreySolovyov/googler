package ru.andreysolovyov.googler.auxiliary;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import ru.andreysolovyov.googler.R;
import ru.andreysolovyov.googler.R.drawable;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.widget.ImageView;

public class ImageLoadTask extends AsyncTask<Void, Void, Bitmap> {

	private String URL;
	private String imageId;
	private LruCache<String, Bitmap> cache;
	private ImageView imageView;

	public ImageLoadTask(String URL, String imageId,
			ImageView imageView, LruCache<String, Bitmap> cache) {
		
		this.URL = URL;
		this.imageView = imageView;
		this.imageId = imageId;
		this.cache = cache;
		
	}

	@Override
	protected Bitmap doInBackground(Void... params) {
		try {
			return BitmapDownloader.downloadBitmap(URL);
		} catch (Exception e) {
			Log.e("HELLO123", "������ �������� �����������");
			e.printStackTrace();
			return null;
		}
	}

	@Override
	protected void onPostExecute(Bitmap result) {
		super.onPostExecute(result);
		if (imageView != null) {
			if (imageId != null && result != null) {
				cache.put(imageId, result);
				imageView.setImageBitmap(result);
			} else {
				imageView.setImageBitmap(BitmapFactory.decodeResource(imageView
						.getContext().getResources(), R.drawable.sample_6));
			}
		}
	}

}